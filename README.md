# npm_package_webapp_gatherer

### Importing

```jsx
import { BAASComponent } from '@koc/npm_package_webapp_gatherer';
```


create develop branch and protect it

### GITLAB CI/CD

#### VARIABLES
```
GITLAB_TOKEN
NPM_TOKEN
CAN_PUBLISH = yes  only on client repos
```

#### SCHEDULES

- variables
```
SCHEDULE_TARGET = SCOPED_VERSION
```


#### TOKENS

Settings -> Access Token -> new
- TOKEN_FOR_CI
- no expi
- Maintainer
- api

COPY

Settings -> CI/CD -> Variables -> new
- GITLAB_TOKEN
- PASTE
- PROTECTED - yes
- MASKED - yes
- Expand - no


SET DEFAULT NPM JS TOKEN to publish first version
After published change npm token with granular one just for this package


https://www.npmjs.com/settings/ -> Access Token -> Generate Granular
- token name -> how to package will be called, taken from the package.json
- @koc/npm_package_webapp_temp1 example

FOR MANUAL NPM
```
λ npm publish --access public
```


NPM_TOKEN -> PASTE