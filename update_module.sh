#!bin/sh

DATE=$(date +"%Y-%m-%d-%H-%M-UTC")
NEW_VERSION_BRANCH=scoped-version-bump-$DATE

git clone "https://${GITLAB_USERNAME}:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git" "${CI_COMMIT_SHA}"
cd "${CI_COMMIT_SHA}"
# Update scoped packages if new version is available
npx npm-check-updates "/^@koc/npm_package_webapp.*$/" -u

# Get diff after packages version check
git diff --name-only > diff.txt

if [ -s diff.txt ]; then
  # There are changes to package versions
  git checkout develop
  npm i # trigger install of new package versions
  git config --global user.email "ci@email.com"
  git config --global user.name "CI/CD Version Bump"
  git checkout -B $NEW_VERSION_BRANCH
  git add .
  git commit -m "Scoped packages version bump"
  git push \
  origin $NEW_VERSION_BRANCH \
  -o ci.skip \
  -o merge_request.create \
  -o merge_request.title="New version of scoped packages available" \
  -o merge_request.target=develop
  # exit 1 # Fail job
else
  # There are NO changes to package versions
  exit 0 # Pass job
fi